package jp.alhinc.uemura_mao.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		// コードの都合上先にmapとListの宣言だけしておく
		Map<String, String> branchLst = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		// 支店定義ファイルの読み込み
		BufferedReader branchLstBr = null;
		try {
			File branchLstFile = new File(args[0], "branch.lst");
			branchLstBr = new BufferedReader(new FileReader(branchLstFile));
			String line;

			// 支店定義ファイルが存在するかの確認
			if (!branchLstFile.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			// 支店定義ファイルの行がなくなるまで処理をループ
			while ((line = branchLstBr.readLine()) != null) {
				String[] names = line.split(",", -1);
				if (names.length != 2 || !(names[0].matches("^\\d{3}$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				branchLst.put(names[0], names[1]);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			try {
				branchLstBr.close();
			} catch (IOException e1) {
				System.out.println("予期せぬエラーが発生しました");
			}
		}

		// 売上ファイルのファイル名条件を指定する
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String filename) {
				return new File(dir, filename).isFile() && filename.matches("^\\d{8}.rcd$");
			}
		};

		// 指定した条件のフィルタを作成
		File[] salesFilesList = new File(args[0]).listFiles(filter);

		ArrayList<Integer> fileNumbers = new ArrayList<>();

		// ファイル名の連番チェック用にファイル名をListへ追加
		for (int k = 0; k < salesFilesList.length; ++k) {
			String fileName = salesFilesList[k].getName().substring(0, salesFilesList[k].getName().lastIndexOf('.'));
			int fileNumber = Integer.parseInt(fileName);
			fileNumbers.add(fileNumber);
		}
		// ソートしてからListの前後比較
		Collections.sort(fileNumbers);
		for (int l = 0; l < salesFilesList.length; ++l) {
			int Number = fileNumbers.get(l);
			if (fileNumbers.get(++l) - Number != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		// 売上ファイルの読み込み
		for (int j = 0; j < salesFilesList.length; ++j) {
			BufferedReader salesFileBr = null;
			try {
				File salesFile = salesFilesList[j];
				salesFileBr = new BufferedReader(new FileReader(salesFile));

				// 読み込んだ行の定義
				String brachKey = salesFileBr.readLine();
				String sales = salesFileBr.readLine();

				// 抽出した売上額をLong型に変換
				Long longsales = Long.parseLong(sales);

				// 3行以上あるときにエラーを返す
				String line3;
				line3 = salesFileBr.readLine();
				if (line3 != null) {
					System.out.println(salesFile + "のフォーマットが不正です");
					return;
				}

				// brachKeyがbranchLstに存在しなければエラーを返す
				if (!branchLst.containsKey(brachKey)) {
					System.out.println(salesFile + "の支店コードが不正です");
					return;
				}

				// branchSalesから同じキーの要素があるか検索
				if (branchSales.containsKey(brachKey)) {
					Long samesales = branchSales.get(brachKey);

					// 要素があったらbranchSalesにあるvalueと合計してput
					Long sumsales = longsales + samesales;
					branchSales.put(brachKey, sumsales);

				// 同じキーの要素が無い時はそのままput
				} else {
					branchSales.put(brachKey, longsales);
				}

				Long branchSalesValue = branchSales.get(brachKey);
				String totalSales = Long.toString(branchSalesValue);

				// branchSalesのvalueが10桁越えていたらエラー
				if (totalSales.matches("^\\d{10,}$")) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				try {
					salesFileBr.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
		// 結果出力
		File salesOutPut = new File(args[0], "branch.out");
		PrintWriter pw = null;

		// Mapからキーと値を出力
		for (String key : branchLst.keySet()) {
			try {
				pw = new PrintWriter(new FileWriter(salesOutPut, true));
				pw.println(key + " , " + branchLst.get(key) + " , " + branchSales.get(key));
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				pw.close();
			}
		}
	}
}